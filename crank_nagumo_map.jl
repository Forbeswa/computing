using LinearAlgebra, Plots, DataFrames, Distributed, Printf

function plot_initial_condition()
  x = range(0, step = 0.1, stop = 20)
  IC = x -> (1 + exp(-(2 - x) / sqrt(2)))^-1
  u0 = IC.(x)

  s_title = "reduced Nagumo initial condition"
  output_name = string(s_title, ".png")
  output_loc = joinpath(pwd(), output_name)
  plot(x, u0, title = s_title, xlabel = "x", ylabel="u(x,0)", label = false, dpi=300)
  savefig(output_loc)
  return true
end

function plot_reaction_term()
  u = range(0, step = 0.01, stop = 1)
  fu = reaction_term(u, 0.376)

  s_title = "reduced Nagumo reaction term"
  output_name = string(s_title, ".png")
  output_loc = joinpath(pwd(), output_name)
  plot(u, fu, title = s_title, xlabel = "u", ylabel="f(u)", label = "alpha = 0.375", legend = :topleft, dpi=300)
  savefig(output_loc)
  return true
end

# definitions

function reaction_term(U, alpha)
  U .* (1 .- U) .* (U .- alpha)
end

function calc_new_step(B_inv, P, u0, D, k, t_steps, M, alpha)
  steps = zeros(Float64,t_steps+1,M+1)
  steps[1,:] = u0
  U = u0

  for i in range(2, step = 1, stop = t_steps)
    F = reaction_term(U, alpha)
    E = P * U + F * k * 2
    U = B_inv * E # new time step
    global steps[i,:] = U
  end
return steps
end

function initial_condition(x)
  IC = x -> (1 + exp(-(2 - x) / sqrt(2)))^-1
  u0 = IC.(x)
  return u0
end

function crank(M, mu, D, boundary)
  I_ = Matrix(1.0I, M + 1, M + 1)
  A = [1 -2 1; 1 -2 1; 1 -2 1]
  Diagonal(A)

  dl = [1.0 for i = 0:M-1]
  d = [-2.0 for i = 0:M]
  du = [1.0 for i = 0:M-1]
  C = Tridiagonal(dl, d, du)

  B = 2 * I_ - mu * D * C
  # adjust for boundary conditions
  if (boundary == "HN")
    B[1, 2] = B[1, 2] * 2 * D
    B[size(B, 1), size(B, 1)-1] = B[size(B, 1), size(B, 1)-1] * 2 * D
  end
  B_inv = inv(B)

  P = 2 * I_ + mu * D * C
  if (boundary == "HN")
    P[1, 2] = P[1, 2] * 2 * D
    P[size(P, 1), size(P, 1)-1] = P[size(P, 1), size(P, 1)-1] * 2 * D
  end
  return B_inv, P
end

function calc_speed(df, h, k, M, alpha, D)
  data = df
  start_time = Int(floor(ncol(data)*0.50))
  end_time = Int(floor(ncol(data)*0.75))
  starting_df = df[:, [start_time]]
  end_df = df[:, [end_time]]

  start = starting_df[starting_df[:, 1] .>= 0.5, :]
  end_ = end_df[end_df[:, 1] .>= 0.5, :]
  start_space = nrow(start)
  end_space = nrow(end_)
  time_interval = (end_time - start_time)*k
  distance_interval = (end_space - start_space)*h
  speed = distance_interval / time_interval
  speed_df = DataFrame("StartPoint" => [start_space], "EndPoint" => [end_space],
  "TimeInterval" => [time_interval], "DistanceInterval" => [distance_interval],
  "Speed" => [speed], "M" => [M], "k" => [k], "alpha" => [alpha],
  "D" => [D])
  #speed_df = rename!(speed_df, :param => param)
  return speed_df
end

function process(; M = 512, D = 1, alpha = 0.10)
  a = 0
  b = 20
  T = 10
  t_steps = 10^4
  local solutions = Dict{String, DataFrame}()
  local speed = Dict{String, DataFrame}()
  #key = round(alpha, digits = 5)
  # Return an iterator over the product of several iterators. 
  # Each generated element is a tuple whose ith element comes from the ith argument iterator. 
  # The first iterator changes the fastest.
  map(Iterators.product(M, D, alpha)) do (M, D, alpha)
        keystr = @sprintf("M = %0.5g, alpha = %0.5g, D = %0.5g", M, alpha, D)

        k = T / t_steps
        h = (b - a) / M
        mu = k / h^2
        boundary = "HN"

        x = [h * i for i = 0:M]
        t = [k * i for i = 0:t_steps]

        # initial condition
        u0 = initial_condition(x)

        # crank matrices
        local B_inv, P = crank(M, mu, D, boundary)
        # time step
        local time_steps = calc_new_step(B_inv, P, u0, D, k, t_steps, M, alpha)
        p_title = string("M = ", M, " ", "alpha = ", alpha, " ",
        "k = ", k, " ", "diff = ", D)
        Plots.contourf(x, t, time_steps, fill=true, c=:vik, title=p_title, xlabel="x", ylabel="t", dpi=300)

        # saves the current plot:
        global output_name = string(p_title, ".png")
        global subfolder = joinpath(pwd(), string(param))
        if ispath(subfolder) == 0
          mkdir(subfolder)
        end
        output_loc = joinpath(subfolder, output_name)
        savefig(output_loc)
        complete = DataFrame(Transpose(time_steps),:auto)
        solutions[keystr] = complete
        speed_key = calc_speed(complete, h, k, M, alpha, D)
        speed[keystr] = speed_key
        return nothing
  end
  return solutions, speed
end

@time begin
param = "reaction" # reaction, mesh, diffusion
r_start = 0
r_end = 0.38
r_length = 2
parameter_range = range(r_start, r_end, length=r_length)
#parameter_range = [0.4]

if param == "reaction"
  u_steps, speed_steps = process(; alpha = parameter_range, D = [1.0], M = [1024])
end
if param == "mesh"
  u_steps, speed_steps = process(; M = parameter_range, D = [1.0], alpha = [0.10])
end
if param == "diffusion"
  u_steps, speed_steps = process(; D = parameter_range, alpha = [0.10], M = [512])
end

data = reduce(vcat, values(speed_steps))

s_title = string("speed vs ", param)
output_name = string(s_title, ".png")
subfolder = joinpath(pwd(), string(param))
output_loc = joinpath(subfolder, output_name)

x = data[!, "alpha"]
y = data[!, "Speed"]
scatter(x,y, title = s_title, xlabel = string(param, " term"), ylabel="average speed of traveling wave u(x,t)",
dpi=300, markersize = 2, markerstrokewidth = 0, label=false)
savefig(output_loc)
end
