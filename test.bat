ECHO OFF

IF NOT DEFINED LOG_ROOT (set LOG_ROOT = C:\logs)

IF DEFINED DEF_ENV (CALL conda.bat activate %DEF_ENV%) ELSE (ECHO No default env set)

IF NOT "%1"=="" (
	SET DB=%1
)	ELSE (
	SET DB=PRODDB
)

IF "%2"=="" (
	SET CFG_MAP=..\cfg\mapping.xlsx
)	ELSE (
	SET DB=%2
)


set PYTHONPATH=..\src;

ECHO ON

python ..\src\neisocal.py ^
 --db %DB% ^
 --mapping "%CFG_MAPPING%" ^
 
IF DEFINED DEF_ENV (CALL conda.bat deactivate)
