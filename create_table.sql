USE [RetailPricing]
GO

SET_ANSI_NULLS ON
GO

SET QUOTED_IDENTIIFIER ON
GO

SET ANSI_PADDING ON
GO

IF OBJECT_ID('autopricing.UtilityComponent', 'U') IS NOT NULL
	drop table [autopricing].[UtilityComponent];


CREATE TABLE autopricing.UtilityComponent
(
	UtilityComponentId int IDENTITY (1,1) PRIMARY KEY,
	UtilityShortName varchar(255)		NOT NULL,
	StartDate datetime2,
	Rate decimal(16,7),
	CreateDate	datetime2
		constraint DFT_UtilityComponent_CreateDate default getdate(),
	CreateUser	varchar(50),
		constraint DFT_UtilityComponent_CreateUser default suser_sname()
)
GO


IF OBJECT_ID('autopricing.[UtilityComponent]', 'UQ') IS NOT NULL
alter table [autopricing].[UtilityComponent]
	drop constraint AK_UtilityComponent
	

ALTER TABLE autopricing.UtilityComponent
	ADD CONSTRAINT AK_UtilityComponent
		UNIQUE (MtmDate, UtilityShortName, Component, LoadZone);
GO


