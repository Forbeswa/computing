import argparse
import sys
import logging
import os
import sqlalchemy as sa
import requests
import io

def connect_to_db(connection_string, password=None):
    known_envs = {'proddb': r'mssql+pyodbc://@PRODDB',
                  'prod2db': r'mssql+pyodbc://user_name:{password}@PROD2DB',
                  'prod3db': r'oracle+cx_oracle://user_name2:{password}@PROD3DB'}
    if connection_string.lower() in known_envs:
        env = known_envs[connection_string.lower()]
    else:
        env = connection_string
    if password:
        env = env.format(password=password)
    return sa.create_engine(env)
       

def main(argv=None):
    logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')
    
    parser = argparse.ArgumentParser(description = __doc__)
    parser.add_argument('--log', default='INFO')
    parser.add_argument('--inputDir', default='C:\data')
    parser.add_argument('--db', default='QAB')
    parser.add_argument('--mapping', default='C:\data\mapping.xlsx')
    parser.add_argument('--product_type', default=False)
    avail_switches = ['coffee', 'pde', 'pizza']
    switches_help = 'which process to run, comma-separated. Possible switches are {0}'.format(avail_switches)
    parser.add_argument('--switches', help=switches_help, required=True)
    
    args = parser.parse_args(argv)
    start = datetime.now()
    sql_conn = connect_to_db(args.db)
    
    switches = args.switches.split(',')
    unknown_switch = [s for s in switches if s not in avail_switches]
    if (len(unknown_switch)) > 0:
    	logging.error('do not know switches {0}'.format(unknown_switch))
    	logging.error('known switches {0}'.format(avail_switch))
    root_dir = args.inputDir
    
    attempt = process(sql_conn, args.mapping, args.product_type)
    time_taken = datetime.now() - start
    logging.info('time taken {0}'.format(time_taken))
    return True

if __name__ == '__main__':
    main(sys.argv[1:])    

class CurvesClass():
    
    def __init__(self, mtm_dict, SQL_server):
        
        data = mtm_date['test']
        
        self.energy = return_energy(SQL)


# create new environment

conda create --name environment_name
activate environment_name
conda install matplotlib pandas numpy pyodbc scikit-learn scipy sqlalchemy xlrd xlsxwriter etc.

pip install emcee
pip install geopy

# masks

nyiso_mask = data['LoadZone'].isin(('CONED', 'NIMO'))
data.loc[nyiso_mask, 'ISO'] = 'NYISO'
data['Utility'] = None
data.loc[nyiso_mask, 'Utility'] = data['LoadZone']

pjm_mask = data['ISO'].isnsull()
data.loc[pjm_mask, 'ISO'] = 'PJM'

data.loc[data['LoadZone'] == 'NIMO', 'LoadZone'] = data['ProposedLoadZone']

summer_mask = data['Month'].isin(summer_list)
summmer = data[summer_mask].copy()

nyiso_mask = ancillary_data["LoadZone"].isin(("CONED", "NIMO"))
ancillary_data.loc[nyiso_mask, "ISO"] = "NYISO"
ancillary_data["Utility"] = None
ancillary_data.loc[nyiso_mask, "Utility"] = ancillary_data["LoadZone"]
ancillary_data.loc[nyiso_mask, "LoadZone"] = None

pjm_mask = ancillary_data["ISO"].isnull()
ancillary_data.loc[pjm_mask, "ISO"] = "PJM"

used_planning_meters_mask = planning_meters['Not Used'].isnull()
test = planning_meters[used_planning_meters_mask]

test.loc[(test['Rate Subclass'] == 'R') & (test['Usage'] < 450), 'LoadProfile'] = 'DEFAULT_PECO_PECO_R111'

calibration_averages = avg_avg_cap_df.join(avg_annual_usage_df, how='inner')
calibration_averages = calibration_averages.join(avg_total_premises_df, how='inner')

#all_smd_data['HourStarting'] = all_smd_data['HourEnding'].astype(int) - 1 
#all_smd_data['datetime'] = all_smd_data['Date'] + pd.to_timedelta(all_smd_data['HourStarting'], unit='h')

summer_mask = test['Month'].isin((6,7,8))
new_test = test[summer_mask]

cdd_zero_mask = new_test['CDD'].isin((0,1))
new_test = new_test[~cdd_zero_mask]

day_of_week_mask = new_test['DayOfWeek'].isin((5,6))
new_test = new_test[~day_of_week_mask]

nimo_df.loc[nimo_df['LoadZone'] == 'NIMO', 'LoadZone'] = nimo_df['ProposedLoadZone']

# datetime stuff

data['HourStarting'] = data['HourEnding'].astype(int) - 1
data['datetime'] = data['Date'] + pd.to_timedelta(data['HourStarting'], unit = 'h')

data['Date'] = pd.to_datetime(data[['Year', 'Month', 'Date']])

keys = pd.data_range('2019-01-01', '2019-12-01', freq='MS').strftime('%m-%d-%y').tolist()

test = df.resample('D', on = 'datetime').sum()

df = pd.date_range(start, periods = 365, freq='D').strftime(%m-%d-%y')
waht = pd.date_range(start = min_date, end = max_date)

delta = df['ToDate'] - df['FromDate']
df['Interval'] = delta.dt.days
df['AvgDailyUsage'] = df['Usage'] / (delta.dt.days + df['Inclusion'])

new['Month'] = pd.DatetimeIndex(new['Date']).month
df['DaysInPeriod'] = (df['Todate'] - df['fromdate'])/pd.offsets.Day(1)

data['datetime'].dt.floor('Min')

# calendarize

acc['TestFromDate'] = acc['FromDate'].shift(-1)
acc.loc[acc['check'] == True, 'Inclusion'] = 0

new = pd.merge(what, current_acc, on = [key])
new = new.query('Date >= FromDate and Date <= ToDate').copy()

# misc

data['AccountNumber'] = data['AccountNumber'].str.zfill(10)

https://stackoverflow.com/questions/13061478/change-a-pandas-dataframe-column-value-based-on-another-column-value

method 1:
cond = df.ORG2 == 'ESBL'
df.ORG1[cond] = df.ORG2[cond]

method 2:
df.loc[df['ORG2'] == 'ESBL', 'ORG1'] = df['ORG2']
Or, if you need a copy, without modifying original df, you can use .mask()
df.mask(df['ORG2'] == 'ESBL', df['ORG2'], axis=0)

np.where(df.c1 == 8,'X',df.c3)

data['MeterNumber'] = data['MeterNumber'].str.replace(',', '')
data['MeterNumber'] = data['MeterNumber'].str.replace('\s+', '') # white space

df['MeterNumber'].fillna(df['AccountNumber'], inplace = True)

full = full.dropna(axis = 0, how = 'any')

df2 = df[1:][:]
df2 = df2.drop('level_1', axis = 1)
cap = cap.drop(cap.index[0:9])

acc_list = df['ColumnName'].drop_duplicates().values.tolist()

df = df[df['Price'].notna()].copy()

data.loc[(test['RateSubClass'] == 'R') & (test['Usage'] < 450), 'LoadProfile'] = 'DEFAULT 123'

combined = df1.merge(df2, on='col1', how='outer', indicator=True)
combined[combined._merge != 'both']

load = df.groupby(['Date'])['MW'].mean()

new = new.query('SCEDTimestamp >= StartDate and SCEDTimestamp <= EndDate').copy()

mask = new['Column'].str.contains(',')
a_list = terms.split(',')
map_object = map(int,a_list)
list_of_integers = list(map_object)

final['Location'] = final.index

check.loc[(pd.isnull(check['EndDate'])), 'UpdatedUsage'] = True
null = df[df['SecondKC'].isnull()].copy()
    
# plotting

output_dir = r'C:data_science'
kfc = (premise, daytype, model)
output_name = ' '.join(kfc)
output_loc = os.path.join(output_dir, output_name + '.png')

plt.figure(figsize = (11,5))
plt.plot(X, y, 'b.', color = 'dodgerblue', markersize = 2)
plt.plot(X, y_pred, color = 'red', linewidth = 1, label = 'LinearRegression')
plt.xlabel('AvgTempF', fontize = 10)
plt.ylabel('kWh', rotation = 0, fontsize=10, labelpad=20)
plt.legend(loc = 'upper left', fontsize = 10)
plt.grid()
plt.savefig(output_loc, title = model, dpi = 300, bbox_inches = 'tight')
plt.close

# logging

logging.info('Finished for {0} {1}'.format(utility, product))

# exception

try:
    success = func()
except(ValueError, KeyError):
    logging.info('Finished for {0} {1}'.format(utility, product))
    raise ValueError()
    
    
try:
    success = funct()
except(ValueError, KeyError):
    success = False
    continue

# queries

sql = '''
select MAX(MtmDate) AS Latest FROM RetailPricing.autopricing.Forecast
'''
Forecast_max_date = pd.read_sql(sql, SQL_Server)
latest_dates_dict['Forecast'] = Forecast_max_date.loc[0, 'Latest']

sql = '''
select * FROM RetailPricing.autopricing.Forecast where MtmDate = '{Latest}'
'''.format(Latest = mtm_date)

delete_table = '''delete from RP.auto.ptc'''
connection = SQL_server.raw_connection()
cursor = connection.cursor()
cursor.execute(delete_table)
connection.commit()
cursor.close()

# API

passed = {'user':username, 'password':password}
r = requests.post('http://www.test.com/api/authentic', data = passed)
cookies = r.cookies

urldata = requests.get(url_string, cookies=cookies).content
Data = pd.read_csv(io.StringIO(urldata.decode('utf-8')), delimiter='|')

# simple scrape

url = 'www.test.com/file.csv'
urlData = requests.get(url).content
cols_use = np.arange(0,22)
df = pd.read_csv(io.StringIO(urlData.decode('utf-8')),delimiter=',',skiprows=2,header=None,usecols=cols_use

# site scrape

dl_folder = 'blah'
chromeOptions = webdriver.ChromeOptions()
chromeOptions.add_experimental_option('prefs',{'download.default_directory':dl_folder})
driver = webdrive.Chrome(options=chromeOptions) 

browser = driver
browser.get(url)
logging.debug(browser.title)
user_box = browser.find_element_by_name('LoginEmail')
user_box.send_keys(username)

button_go = browser.find_element_by_class_name('submit-button')
button_go.click()

# dictionary

res = dict(tuple(d.groupby('calibrationmeter')))

mydict = dict(zip(ny_zip_zone_mapping.ServiceZip, ny_zip_zone_mapping.ProposedLoadZone))
query_df['LoadZone'] = query_df['ServiceZip'].replace(mydict, inplace=False)

# set stuff

table_intersection = list(set(non_ercot_var_meter).intersection(set(ptc_table_meter)))
table_missing = list(set(non_ercot_var_meter) - set(ptc_table_meter))
ptc_miss_fix_int = list(set(table_missing).intersection(set(fixed_meter)))
ptc_miss_fix = list(set(table_missing).difference(set(ptc_miss_fix_int)))

df['NegU'] = df['kWh'].apply(lambda x: True if x < 0 else False)
